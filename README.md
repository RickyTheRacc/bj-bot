# BJ Bot

BJ Bot is the second life of the "Bad Janet" bot for the WFM server.
https://gitlab.com/wfm-discord/bad-janet

It is built using Discord.js, and is ESM.


# Developing

## Slash Commands
`/src/commands`  
All slash command should go in `/src/commands`  
One command per file, please.

## Cron Jobs
`/src/cron`  
One cron job per file.

## Events
`/src/events`  
A place for events. One file per event. If you need `onMessageCreate`, there's a `messageCreateListeners` folder to prevent the messageCreate.mjs file from becoming too large with time.  
One event per file. 

## "Scripts" & Message Listeners
`/src/scripts`  
"Scripts" is my hold-over from Hubot, and are listeners for specific regex. The code that actually handles listening and applying the scripts is `/src/events/messageCreateListeners/handleScripts.mjs`.  
Multiple scripts per file are allowed and will work

If you need something more complicated than applying regex to a message, you can put your own message listeners in `/src/events/messageCreateListeners/`, too.

# Utilities
`/src/utils`  
For reusable code that will go into more than one file.  
Multiple functions or whatever per file is fine.
