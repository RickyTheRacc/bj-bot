import { config } from 'dotenv';
config(); //for dotenv
import { REST } from 'discord.js';
import { Routes } from 'discord.js';
import {importModulesInDir} from "../src/utils/importModulesInDir.mjs";

const commands = []
for await (const commandModule of importModulesInDir('commands', { recursive: true })) {
  commands.push(commandModule.data.toJSON());
}

const rest = new REST({ version: '10' }).setToken(process.env.DISCORD_TOKEN);

rest.put(Routes.applicationCommands(process.env.DISCORD_CLIENT_ID), { body: commands })
  .then((data) => console.log(`Successfully registered ${data.length} application commands.`))
  .catch(console.error);
