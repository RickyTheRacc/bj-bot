
const role1Id = process.env.REQUIRED_ROLE_1;
const role2Id = process.env.REQUIRED_ROLE_2;
const role3Id = process.env.REQUIRED_ROLE_3;
const channelId = process.env.WELCOME_CHANNEL_ID; // replace with your channel ID

const guildMemberAdd = {
  name: 'guildMemberAdd',
  async execute(member) {
    try {
      // 15 minutes to poke at someone about their role
      const firstAlertTimer = 900000;

      // Schedule a timeout to check if the member has chosen one of the roles within 1 hour
      const timeout = setTimeout(async () => {
        try {
          // Wait for the member to fully load before proceeding
          await member.fetch();

          if (member.roles.cache.has(role1Id) || member.roles.cache.has(role2Id) || member.roles.cache.has(role3Id)) {
            // shrug I don't feel like rewriting the conditional
          } else {
            // Send a reminder message to the server welcome channel
            const channel = member.guild.channels.cache.get(channelId);
            try {
              await member.guild.members.fetch(member);
              await channel.send(`Hi ${member}, we noticed you haven't chosen `
              + 'a role yet from the <#928037167677181955> channel to agree to '
              + 'our rules and join the rest of the server. You need to choose '
              + 'a role to agree to the server rules and to stay on the server. '
              + 'Do you need any help? ');
            } catch (error) {
              if (error.code === 10007) {
                // Member is no longer in the guild, do nothing
                return;
              }
              console.error(`Error while fetching member: ${error.message}`);
            }
          }
        } catch (error) {
          console.error(`Error while sending reminder message: ${error.message}`);
        }
      }, firstAlertTimer);
      // Store the reminder timeout in the member object for later reference
      member.reminderTimeout = timeout;
    } catch (error) {
      console.error(`Error while scheduling timeout: ${error.message}`);
    }
  },
};

export default guildMemberAdd;
