
import { initBrain, brain, brainKeyExists } from './brain.mjs';
import { removeRoleByName } from './users.mjs';
const grounded_role = process.env.GROUNDED_ROLE;

/**
 * Utilities to help make grounding code happen in more than one place
 * @type {Object}
 */
export const groundingUtils = {
  testMinutes: function(minutes) {
    // test okay if minutes are less than 12
    return minutes <= 12;
  },

  /**
   * Removes grounding role from a user and clears their brain object
   * @param string user_id
   */
  clearGrounding: async function(user_id, guild) {
    await initBrain();
    // remove the grounded role
    await removeRoleByName(user_id, guild, grounded_role);

    // remove the grounding from the brain if it exists
    if (brain.data.grounding[user_id]) {
      delete brain.data.grounding[user_id];
      brain.write();
    }
  },

  isUserAlreadyGrounded: function(user_id) {
    brainKeyExists('grounding');
    return brain.data.grounding[user_id];
  },

  groundUser: function(user_id, minutes_todo, minutes_minimum, percentage) {
    // make sure user has a grounding key in the brain
    brainKeyExists('grounding', user_id);
    if (brain.data.grounding[user_id].minutes_todo) {
      return groundingUtils.increaseGrounding(user_id, minutes_todo, minutes_minimum, percentage);
    } else if (minutes_todo) {
      return groundingUtils.newGrounding(user_id, minutes_todo, minutes_minimum, percentage);
    }
  },

  newGrounding: function(user_id, minutes_todo, minutes_minimum, percentage) {
    // make sure user has a grounding key in the brain
    brainKeyExists('grounding', user_id);
    let reply_message = '';
    if (minutes_todo) {
      // received minutes so is going to set the grounding
      groundingUtils.calculateGroundingData(user_id, minutes_todo, minutes_minimum, percentage);
      reply_message = groundingUtils.generateGroundingMessage(user_id);
    } else {
      // Bot was unable to figure out the grounding, so is just gonna do something small
      groundingUtils.calculateGroundingData(user_id, 1, 0, 0);
      reply_message = 'Wait, how many minutes? I didn\'t understand your command or you forgot to tell me how many minutes the user is grounded for.\nI guess a 1 minute grounding with no restrictions will do...\n';
      reply_message += groundingUtils.generateGroundingMessage(user_id);
    }
    return reply_message;
  },

  calculateGroundingData: function(user_id, minutes_todo, minutes_minimum, percentage) {
    brainKeyExists('grounding', user_id);
    brain.data.grounding[user_id] = {
      'minutes_todo': minutes_todo,
      'minutes_min': minutes_minimum || 0,
      'minutes_completed': 0,
      'minimum_percentage': percentage || 0
    };
    brain.write();
  },

  generateGroundingMessage: function(user_id) {
    let groundingData = brain.data.grounding[user_id];
    let reply_message = `**<@${user_id}> is grounded!**\n`;
    reply_message += `Minutes to complete: ${groundingData.minutes_todo}\n`;

    if (groundingData.minutes_min) {
      reply_message += `Minimum grounding session: ${groundingData.minutes_min} minutes\n`;
    } else {
      reply_message += 'No minimum grounding session set.\n';
    }

    if (groundingData.minimum_percentage) {
      reply_message += `Percentage minimum: ${groundingData.minimum_percentage}%\n`;
    } else {
      reply_message += 'No minimum percentage of completion set.\n';
    }

    return reply_message + '\n\n' + groundingMessages.howTo;
  },

  increaseGrounding: function(user_id, minutes_todo, minutes_minimum, percentage) {
    // make sure user has a grounding key in the brain
    brainKeyExists('grounding', user_id);
    let reply_message = `**<@${user_id}> is already grounded!** Your grounding is `
      + 'updating their current requirements.';

    if (minutes_minimum && minutes_todo && minutes_minimum > minutes_todo && minutes_minimum > +brain.data.grounding[user_id].minutes_todo + +minutes_todo) {
      reply_message += `You told me to ground them for ${minutes_todo} `
        + `minutes and they have to do a minimum of each grounding session for ${minutes_minimum} `
        + 'minutes, but that\'s not quite right. I\'m going to assume you meant the reverse.';
      let temp = minutes_todo;
      minutes_todo = minutes_minimum;
      minutes_minimum = temp;
    }

    if (minutes_todo) {
      brain.data.grounding[user_id].minutes_todo = +brain.data.grounding[user_id].minutes_todo + +minutes_todo;
      reply_message += `\nMinutes grounded updated to: ${brain.data.grounding[user_id].minutes_todo}`;
    } else {
      reply_message += `\nMinutes grounded: ${brain.data.grounding[user_id].minutes_todo}`;
    }

    if (minutes_minimum) {
      if (minutes_minimum > brain.data.grounding[user_id].minutes_min) {
        brain.data.grounding[user_id].minutes_min = minutes_minimum;
        reply_message += '\nMinimum grounding session now: ';
      } else {
        reply_message += '\nMinimum grounding session unchanged: ';
      }
    } else {
      reply_message += '\nMinimum grounding session: ';
    }
    reply_message += brain.data.grounding[user_id].minutes_min + ' minutes';

    if (percentage) {
      if (percentage > brain.data.grounding[user_id].minimum_percentage) {
        reply_message += '\nPercentage minimum required now: ';
        brain.data.grounding[user_id].minimum_percentage = percentage;
      } else {
        reply_message += '\nPercentage minimum required unchanged: ';
      }
    } else {
      reply_message += '\nPercentage minimum required: ';
    }
    reply_message += brain.data.grounding[user_id].minimum_percentage + '%';

    reply_message += '\nGrounding completed so far: ';
    reply_message += brain.data.grounding[user_id].minutes_completed;
    reply_message += ' minutes leaving **';
    reply_message += brain.data.grounding[user_id].minutes_todo - brain.data.grounding[user_id].minutes_completed;
    reply_message += '** minutes to complete. ';

    brain.write();
    return reply_message + '\n\n' + groundingMessages.howTo;
  },
}

/**
 * This is a place I'm keeping messages so that they can be
 * updated in one place because they're going to be used in
 * more than one place
 */
export const groundingMessages = {
  minMinuteMessage: 'Sorry, the minimum amount of minutes to '
      + 'do at a time has to be 12 or less. '
      + 'The bot can\'t do more than 12 minutes at a time.',


  howTo: 'Type `/ground_me` or `/surprise_me` to serve out your grounding time using <@1029920668428554312>.',

  /**
     * Given a user ID, builds a status message for a user's grounding
     * @param  string user_id
     */
  statusMessage: function(
    user_id
  ) {
    brainKeyExists('grounding');
    const record = brain.data.grounding[user_id];
    if (!record) {
      return `<@${user_id}> is not currently grounded.`;
    }

    let grounded_for = `<@${user_id}> is grounded for ${record.minutes_todo} minutes, `;
    let minimum = record.minutes_min ?
        `with a minimum grounding session of ${record.minutes_min} minutes, ` :
      'with no minimum grounding session, ';
    let percentage = record.minimum_percentage ?
        `and a minimum percentage of ${record.minimum_percentage}%.\n` :
      'and no minimum percentage.\n';
    let completed = record.minutes_completed ?
        `${record.minutes_completed} minutes have been completed.\n` :
        `Nothing has been completed yet so there's still ${record.minutes_todo} minutes to do.`;

    const left = record.minutes_todo - record.minutes_completed;
    let left_msg = left == record.minutes_todo ? '' : `${left} minutes left.`;

    return grounded_for + minimum + percentage + completed + left_msg;
  }
}
