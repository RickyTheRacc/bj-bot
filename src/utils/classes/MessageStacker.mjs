/**
 * Message Stacker
 * Use this when looping to build a message so that it can
 *  stack it up and break it appart appropriately.
 */
class MessageStacker {
  constructor(message, prepend = '') {
    this.message = message;
    this.prepend = prepend;
    this.message_cache = '';
  }

  appendOrSend(message_temp) {
    // Discord character limit of 2000 characters
    if ((this.prepend + this.message_cache + message_temp).length > 2000) {
      this.message.reply(this.prepend + this.message_cache);
      this.message_cache = message_temp;
      this.prepend = '';
    } else {
      this.message_cache += message_temp;
    }
  }

  finalize(append = '', no_message = '') {
    if ((this.prepend + this.message_cache).length === 0
      && no_message.length > 0
    ) {
      this.message.reply(no_message);
    } else if ((this.prepend + this.message_cache).length === 0) {
      this.message.react('🤷‍♀️');
    } else {
      if ((this.prepend + this.message_cache + append).length > 2000) {
        this.message.reply(this.prepend);
        this.message.reply(this.message_cache);
        this.message.reply(append);
      } else {
        this.message.reply(this.prepend + this.message_cache + append);
      }
    }
  }
}

export { MessageStacker };
