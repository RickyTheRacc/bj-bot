
import { initBadJanetBrain } from '../utils/badJanetBrain.mjs';
import Discord from 'discord.js';


export const hasConsent = async function (top_user, sub_user) {
  if (!(top_user instanceof Discord.User)) {
    throw new Error('top_user must be a Discord.js user object');
  }
  if (!(sub_user instanceof Discord.User)) {
    throw new Error('sub_user must be a Discord.js user object');
  }

  const consent_list = await getConsentList(sub_user.id, top_user.id);
  return testConsent(consent_list, top_user.id);
}

/**
 * Same as hasConsent() except it takes IDs instead of objects
 * @param  int top_user_id
 * @param  int sub_user_id
 * @return boolean
 */
export const hasConsentByID = async function (top_user_id, sub_user_id) {
  const consent_list = await getConsentList(sub_user_id, top_user_id);
  return testConsent(consent_list, top_user_id);
}

function testConsent(consent_list, top_user_id) {
  if (consent_list.safeword) {
    return false;
  }
  if (consent_list.everyone) {
    return true;
  }
  if (consent_list[top_user_id]) {
    return true;
  }
  return false;
}


async function getConsentList(sub_user_id) {
  const badJanetBrain = await initBadJanetBrain();
  if (badJanetBrain.data.consent[sub_user_id]) {
    return badJanetBrain.data.consent[sub_user_id];
  }
  return false;
}
