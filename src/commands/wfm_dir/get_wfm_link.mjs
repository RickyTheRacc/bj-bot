import { SlashCommandBuilder } from 'discord.js';

import {brain, brainKeyExistsPromise} from '../../utils/brain.mjs';

export const data = new SlashCommandBuilder()
  .setName('get_wfm_link')
  .setDescription('See a user\'s profile link')
  .addUserOption(option => option.setName('user')
    .setDescription('The user you want the link of')
    .setRequired(true));

export async function execute(interaction) {
  await brainKeyExistsPromise('wfm_profiles');
  const user = interaction.options.getUser('user');
  if (brain.data.wfm_profiles[user.id]) {
    interaction.reply(`<@${user.id}>: ` + brain.data.wfm_profiles[user.id]);
  } else {
    interaction.reply(`<@${user.id}> doesn't have a link set.`);
  }
}
