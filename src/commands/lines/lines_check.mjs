import { SlashCommandBuilder } from 'discord.js';
import {lineMessages} from '../../utils/lines.mjs';

export const data = new SlashCommandBuilder()
  .setName('lines_check')
  .setDescription('Check a user\'s lines status')
  .addUserOption(option => option.setName('user')
    .setDescription('The user to check the lines status of')
    .setRequired(true));

export async function execute(interaction) {
  const user = interaction.options.getUser('user');
  const userId = user.id;
  const reply = lineMessages.statusMessage(userId);

  await interaction.reply(reply);
}
