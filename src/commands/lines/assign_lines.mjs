import { SlashCommandBuilder } from 'discord.js';
import {lineUtils} from '../../utils/lines.mjs';
import { hasConsent } from '../../utils/consent.mjs';

export const data = new SlashCommandBuilder()
  .setName('assign_lines')
  .setDescription('Assigns a line to a user')
  .addUserOption(option => option.setName('user')
    .setDescription('The user to assign lines to.')
    .setRequired(true))
  .addIntegerOption(option => option.setName('num_lines')
    .setDescription('The number of lines the user should type.')
    .setRequired(true))
  .addStringOption(option => option.setName('line')
    .setDescription('The line you want the user to write. If left empty, random lines will be generated.')
    .setRequired(false))
  .addStringOption(option => option.setName('personalized_instructions')
    .setDescription('Special instructions for the lines (free text) - please use ||spoiler text|| for anything NSFW.')
    .setRequired(false))
  .addIntegerOption(option => option.setName('goal_time')
    .setDescription('Set a time the user is required to finish their lines in (in seconds).')
    .setMinValue(1)
    .setRequired(false))

export async function execute(interaction) {
  const sender = interaction.user;
  const to_user = interaction.options.getUser('user');
  const lines_todo = interaction.options.getInteger('num_lines');
  const line = interaction.options.getString('line');
  const personalized_instructions = interaction.options.getString('personalized_instructions');
  const command_channel = interaction.channel.id;
  const goal_time = interaction.options.getInteger('goal_time');

  if (!await hasConsent(sender, to_user)) {
    interaction.reply('You do not have consent to set lines for that user.');
    return;
  }

  let reply = `***<@${sender.id}> has given <@${to_user.id}> lines to write!***\n`;
  if (line == null) {
    reply += await lineUtils.assignLinesToUser(to_user.id, to_user.username, sender.id, interaction.guild, lines_todo, line, command_channel, goal_time);
    if (personalized_instructions) {
      reply += `\n\n**You have special instructions:** ${personalized_instructions}`;
    }
    await interaction.reply(reply);
  } else if (line.toLowerCase() != 'cancel') {
    reply += await lineUtils.assignLinesToUser(to_user.id, to_user.username, sender.id, interaction.guild, lines_todo, line, command_channel, goal_time);
    if (personalized_instructions) {
      reply += `\n\n**You have special instructions:** ${personalized_instructions}`;
    }
    await interaction.reply(reply);
  } else {
    await interaction.reply({content: 'You cannot assign someone that line. Try another one.', ephemeral: true});
  }
}
