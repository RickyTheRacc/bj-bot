import { SlashCommandBuilder } from 'discord.js';
import { groundingMessages } from '../../utils/grounding.mjs';

export const data = new SlashCommandBuilder()
  .setName('grounding_check')
  .setDescription('Check a user\'s grounding status')
  .addUserOption(option => option.setName('user')
    .setDescription('The user to check the grounding status of')
    .setRequired(true));

export async function execute(interaction) {
  const user = interaction.options.getUser('user');
  const userId = user.id;
  const reply = groundingMessages.statusMessage(userId);

  await interaction.reply(reply);
}
