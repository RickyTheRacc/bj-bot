import { SlashCommandBuilder } from 'discord.js';
import { hasConsent } from '../../utils/consent.mjs';
import { groundingUtils, groundingMessages } from '../../utils/grounding.mjs';
import { addRoleByName } from '../../utils/users.mjs';

const grounded_role = process.env.GROUNDED_ROLE;

export const data = new SlashCommandBuilder()
  .setName('ground_user')
  .setDescription('Grounds a user')
  .addUserOption(option => option.setName('user')
    .setDescription('The user to ground')
    .setRequired(true))
  .addIntegerOption(option => option.setName('minutes')
    .setDescription('The number of minutes to ground the user for')
    .setRequired(true))
  .addStringOption(option => option.setName('personalized_instructions')
    .setDescription('Special instructions for the grounding (free text) - please use ||spoiler text|| for anything NSFW.')
    .setRequired(false))
  .addStringOption(option => option.setName('instruction_presets')
    .setDescription('Special instructions for the grounding (preset choices)')
    .setRequired(false)
    .addChoices(
      {name: 'Kneel while completing', value: 'Kneel while completing this grounding.'},
      {name: 'Run as surprise me', value: 'Run the groundings as "surprise me" sessions until you\'ve completed this grounding.'},
      {name: 'Shame yourself after grounding', value: 'When your grounding is over, shame yourself and count your way out.'},
      {name: 'Shame after grounding + 1s slowdown for every miss.', value: 'After the grounding, give yourself a slowdown of 1s/miss. Shame yourself. Count out of the corner.'},
      {name: 'Shame for every miss.', value: 'After the grounding, shame yourself once for each miss. Count out of the corner each time.'},
    )
  )
  .addIntegerOption(option => option.setName('minimum_session')
    .setDescription('The minimum session duration in minutes')
    .setRequired(false))
  .addNumberOption(option => option.setName('completion_percentage')
    .setDescription('The required percentage of completion')
    .setRequired(false));

export async function execute(interaction) {
  const sender = interaction.user;
  const to_user = interaction.options.getUser('user');
  const minutes_todo = interaction.options.getInteger('minutes');
  const instructions = interaction.options.getString('instruction_presets');
  const personalized_instructions = interaction.options.getString('personalized_instructions');
  const minutes_minimum = interaction.options.getInteger('minimum_session') || 0;
  const percentage = interaction.options.getNumber('completion_percentage') || 0;

  // check consent
  if (!await hasConsent(sender, to_user)) {
    interaction.reply('You do not have consent to ground that user.');
    return;
  }

  // make sure minimum is 12 or less
  if (!groundingUtils.testMinutes(minutes_minimum)) {
    interaction.reply(groundingMessages.minMinuteMessage);
    return;
  }

  // add grounded role
  await addRoleByName(to_user.id, interaction.guild, grounded_role);

  // update the brain accordingly and reply with what's going on
  let reply = `***<@${sender.id}> has given you a grounding!***\n`;
  reply += await groundingUtils.groundUser(to_user.id, minutes_todo, minutes_minimum, percentage, instructions);

  if (instructions && personalized_instructions) {
    reply += `\n\n**You have special instructions:** \n - ${instructions}\n - ${personalized_instructions}`;
  } else if (instructions) {
    reply += `\n\n**You have special instructions:** ${instructions}`;
  } else if (personalized_instructions) {
    reply += `\n\n**You have special instructions:** ${personalized_instructions}`;
  }

  await interaction.reply(reply);
}
