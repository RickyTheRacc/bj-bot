// Description:
//   manage the lottery for the corner of shame
//
// Commands:
//   ?joinlottery - you can join the lottery
//   ?leavelottery - you can leave the lottery
//   ?listlottery - lists everyone in the lottery
//   ?drawlottery - draw one name from the lottery, finished the lottery and drops all the names
//   ?draw2lottery - use any number of people to draw

import { brain, brainKeyExists } from '../utils/brain.mjs';
import { addShameByUserId, shameUserRecord } from '../utils/shame.mjs';
import _ from 'lodash';
import { config } from 'dotenv';
config(); // Load environment variables from .env file

const shame_role = process.env.SHAME_ROLE;

const suspense = [
  'A few more seconds won\'t hurt anyone, right?',
  'A little more suspense never hurt anybody, right?',
  'Almost done...',
  'Almost there...',
  'And the winner is... (wait for it)',
  'Anticipation is half the fun, right?',
  'Are you on the edge of your seat?',
  'Are you ready for the moment of truth?',
  'Buckle up, it\'s going to be a bumpy ride!',
  'Can you feel the tension building?',
  'Can\'t have a winner without a little suspense, right?',
  'Don\'t blink now!',
  'Don\'t blink, you might miss the moment!',
  'Don\'t get too comfortable...the moment of truth is coming!',
  'Don\'t want to wait anymore? Suffer.',
  'Drumroll please... ',
  'Gosh, I hope this works...',
  'Gotta add some suspense here, ya know.',
  'Hang tight!',
  'Hold on a moment',
  'Hold on to your hats...this is going to be good!',
  'Hold onto your hats!',
  'Hold your breath...',
  'Hold your horses!',
  'Holding my breath...don\'t make me pass out now...',
  'https://tenor.com/view/lafuddyduddy-drum-duck-cute-drum-roll-please-gif-16950134',
  'https://tenor.com/view/lets-see-what-happens-steve-kornacki-msnbc-lets-wait-for-it-were-gonna-wait-for-the-result-gif-19694593',
  'https://tenor.com/view/lottery-lotto-gif-12706150',
  'https://tenor.com/view/psych-shawn-shawn-spencer-james-roday-wait-gif-8535911',
  'https://tenor.com/view/suspense-waiting-wait-mystery-daily-show-gif-22337508',
  'https://tenor.com/view/tea-shade-goth-halloween-you-just-wait-gif-15826642',
  'https://tenor.com/view/wait-for-it-barney-stinson-himym-neil-patrick-harris-gif-6055274',
  'https://tenor.com/view/willy-wonka-excited-smile-the-suspense-gene-wilder-gif-5933521',
  'Huh? Oh! Spaced out there a second.',
  'Hush, hush...don\'t say a word, the moment of truth is almost here.',
  'I could tell you who lost right now, but where\'s the fun in that?',
  'I could use a little help here, anyone want to lend me a hand?',
  'I don\'t want to rush this, especially because I know you probably do.',
  'I don\'t want to spoil the surprise...yet.',
  'I hope you\'re ready for this.',
  'I know you\'re like a kid before Christmas... just can\'t wait to open that present!',
  'I love to make you wait.',
  'I promise it\'s worth the wait.',
  'I\'m almost done, just a couple more shakes.',
  'I\'m almost ready, just one more second...',
  'I\'m counting down the seconds...are you?',
  'I\'m not stalling, I swear!',
  'I\'m not sure if you\'re ready for this yet...',
  'I\'m so excited, and I just can\'t hide it...',
  'If you\'re not excited yet, you will be soon.',
  'It\'s almost time...',
  'Is the anticipation is killing you?',
  'Just a little bit longer.',
  'Just another moment.',
  'Just one more minute...',
  'Just swishing the names around here.',
  'Just when you thought you couldn\'t wait any longer...',
  'Keep calm...I think I\'ve almost got it.',
  'Keep your eyes peeled...',
  'Keep your fingers crossed...',
  'Let me just double check my list...',
  'Let\'s add a little more tension...',
  'Let\'s build up the excitement!',
  'Let\'s just build the suspense a little bit longer.',
  'Oh hold on, I shook up my grocery list instead of the list of names.',
  'Okay, okay, I\'m hurrying. But I don\'t want to make any mistakes!',
  'One. More. Second.',
  'Oops, I dropped a few, give me a moment to make sure they\'re still all here.',
  'Patience is a virtue...a virtue you are putting to the test!',
  'Patience.',
  'Shake shake shaking up the names... ',
  'SHAME SHAME SHAME on you for being so impatient.',
  'Shhh...listen...do you hear that? It\'s the sound of suspense!',
  'Taking my time to savor this moment.',
  'The anticipation is real.',
  'The moment of truth is near.',
  'The moment we\'ve all been waiting for...',
  'The suspense is almost too much to bear!',
  'The suspense is killing me too!',
  'The tension is thick enough to cut with a knife.',
  'This definitely needs a few more seconds of shaking around.',
  'This is it, this is it, this is it!',
  'Tick tock, tick tock, the suspense is killing me too! But I love to make you suffer.',
  'Time seems to slow down when you\'re waiting for something, doesn\'t it?',
  'Time to stir things up a bit!',
  'Wait for iiiiiiiit....',
  'Wait for it...',
  'We\'re almost there.',
  'Who\'s ready for some action?',
  'You ready?',
  'You\'re on the edge of your seat, I can tell!',
  'Your anticipation is palpable.',
];

const chooseString = [
  `Someone needs to go to the ${shame_role} and it's going to be `,
  'I pulled a name and the loser is ',
  'SHAME SHAME SHAME on ',
  'And the loser is ',
  'Looks like we have a loser... it\'s ',
  'The odds were not in your favor ',
  'The lottery gods have spoken, and they\'ve chosen ',
  'The wheel of fortune has stopped spinning, and it\'s landed on ',
  'Better luck next time ',
  'And the unlucky person is ',
  'You can\'t win them all, ',
  'Don\'t feel too bad, it could have been worse... or could it? Either way, you\'re the loser ',
  'You have been chosen by the fates to receive the "loser" label, ',
  'And the winner is... NOT ',
  'Well, we can\'t all be winners. Especially not the loser ',
];


const pause_timer = 7000;
let count = 1;
let keys = [];

/**
 * ?joinlottery
 */
export const joinlottery = {
  regex: /^\?join[lI]ottery/i,
  tag_bot: false,
  execute: async function(message) {
    let brain_data = brain.data;
    let user_id = message.author.id
    const channel = message.channel;
    brainKeyExists('corner_line');
    if (brain_data.corner_line[user_id]) {
      channel.send("You're already in the lottery of shame and you can't join it twice.");
    } else {
      brain_data.corner_line[user_id] = user_id;
      channel.send("You've been added to the lottery.");
    }
    await brain.write();
  }
}

/**
 * ?leavelottery
 */
export const leavelottery = {
  regex: /^\?leavelottery/i,
  tag_bot: false,
  execute: async function(message) {
    let user_id = message.author.id
    brainKeyExists('corner_line');
    const channel = message.channel;
    if (brain.data.corner_line[user_id]) {
      channel.send("Coward. You've been removed from the lottery but you should go write some lines for chickening out.");
      delete brain.data.corner_line[user_id];
      await brain.write();
    } else {
      channel.send("You aren't even entered.");
    }
  }
}

/**
 * ?listlottery
 */
export const listlottery = {
  regex: /^\?listlottery/i,
  tag_bot: false,
  execute: function(message) {
    let brain_data = brain.data;
    let list = '';
    for (let key in brain_data.corner_line) {
      list = list + `<@${key}>, `;
    }
    const channel = message.channel;
    if (list == '') {
      channel.send('No one is entered just yet. Maybe you should add yourself?');
    } else {
      channel.send('Current list of users in the lottery: ' + list);
    }
  }
}

/**
 * ?drawlottery / ?draw2lottery
 */
export const drawlottery = {
  regex: /^\?draw\d*lottery/i,
  tag_bot: false,
  execute: async function(message) {
    let input_message = message.content.match(drawlottery.regex)[0];
    let matched = input_message.match(/\d+/i);
    if (matched) {
      count = parseInt(matched[0]) || 1;
    } else {
      count = 1;
    }

    let brain_data = brain.data;
    let list = '';
    for (let key in brain_data.corner_line) {
      list = list + `<@${key}>, `;
    }
    const channel = message.channel;
    if (list == '') {
      channel.send('No one is entered just yet. Maybe you should add yourself?');
    } else {
      // https://stackoverflow.com/a/49687370
      keys = Object.keys(brain_data.corner_line);
      let length = keys.length;
      if (length < count) {
        count = length;
      }
      if (length == 1) {
        channel.send("Really? There's only one name. Okay. I guess I'll shake up the list that consists of one entire name and choose one.");
        addSuspense(message);
      } else if (length == count) {
        channel.send(`Really? The list only has ${count} names. Not very fair for everyone is it? Okay. I guess I'll shake up the list and choose *everybody*.`);
        addSuspense(message);
      } else {
        channel.send('Okay give me a moment to shake up the ' + length + ` names and I'll choose ${count}.`);
        addSuspense(message);
      }
    }
    await brain.write();
  }
}

function addSuspense(msg) {
  let brain_data = brain.data;
  const channel = msg.channel;
  setTimeout(function() {
    channel.send(_.sample(suspense));
    // 50/50 chance whether there will be a second suspense
    // Unless the count is 1 at which point, suffer
    let x = (Math.floor(Math.random() * 2) == 0);
    if (x || count == 1) {
      setTimeout(function() {
        channel.send(_.sample(suspense));
        setTimeout(function() {
          chooseNames(msg)
            .then(variable => {
              brain_data.corner_line = {};
            });
        }, pause_timer);
      }, pause_timer);
    } else {
      setTimeout(function() {
        chooseNames(msg)
          .then(variable => {
            brain_data.corner_line = {};
          });
      }, pause_timer);
    }
  }, pause_timer);
}


async function chooseNames(msg) {
  for (let i = 0; i < count; i++) {
    keys = Object.keys(brain.data.corner_line);
    let randIndex = Math.floor(Math.random() * keys.length);
    let random_user_id = keys[randIndex];

    shameUserRecord(random_user_id, null, 172800000, 'lottery'); // 48 hours
    await addShameByUserId(msg.guild, random_user_id);

    delete brain.data.corner_line[random_user_id];
    const channel = msg.channel;
    channel.send(_.sample(chooseString) + ` <@${random_user_id}>!`);
    await brain.write();
  }
}
